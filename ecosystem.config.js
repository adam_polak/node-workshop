module.exports = {
  apps: [
    {
      name: 'rates-producer',
      script: 'src/services/rates/index.js'
    },
    {
      name: 'api',
      script: 'src/services/api/index.js',
      watch: true
    }
  ]
}