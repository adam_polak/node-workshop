const NotFoundError = require('../../errors/not-found.error');

const createCurrencyViewModel = (currency) => {
  return {
    id: currency.id
  }
}

const handleGetRates = (currencyRepository) => async (req, res, next) => {
  const currencies = await currencyRepository.findOne({
    relations: ['rates'],
    order: {
      createDate: 'DESC'
    }
  });

  if (!currencies) {
    return next(new NotFoundError('Currencies not found'));
  }

  res.json(createCurrencyViewModel(currencies));
};

module.exports = {
  handleGetRates
}