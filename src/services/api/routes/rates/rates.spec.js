const request = require('supertest');
const { createServer } = require('../../server/server');
const { Currency } = require('../../../../models/currency/currency.model');
const { Rate } = require('../../../../models/rate/rate.model');
const { Joi } = require('celebrate');

describe('Rates API routes', () => {
  it('returns 404 when currency not found', () => {
    return request(createServer({
      currencyRepository: {
        findOne: () => null
      }
    }))
      .get('/rates')
      .expect(404);
  })

  it('returns 200 when currency found', () => {
    const currency = new Currency('currency-id', 'PLN', new Date(), [
      new Rate('rate-id', 'PLN', 0)
    ]);

    return request(createServer({
      currencyRepository: {
        findOne: () => currency
      }
    }))
      .get('/rates')
      .expect(200)
      .then(res => {
        expect(res.body).toEqual({
          id: 'currency-id'
        })
      })
  })
});