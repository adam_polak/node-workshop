const express = require('express');
const { handleGetRates } = require('./get-rates');

const createRatesRouting = (currencyRepository) => {
  const router = express.Router();

  router.get('/', handleGetRates(currencyRepository));

  return router;
};

module.exports = {
  createRatesRouting
};