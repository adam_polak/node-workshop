const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const helmet = require('helmet');
const {celebrate, errors, Joi} = require('celebrate');
const { createRatesRouting } = require('../routes/rates');
const NotFoundError = require('../errors/not-found.error');

const createServer = (container) => {
  const server = express();

  server.use(morgan('dev'));
  server.use(express.json());
  server.use(cors());
  server.use(helmet());

  server.use('/rates', createRatesRouting(container.currencyRepository));

  server.use('*', (req, res, next) => next(new NotFoundError('Page not found')));

  server.use(errors());
  server.use((error, req, res, next) => {
    return res.status(error.status).json({error: error.message})
  });

  return server;
};

module.exports = {
  createServer
};