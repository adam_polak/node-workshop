require('dotenv').config();
const typeorm = require('typeorm');
const { createServer } = require('./server/server');
const { Currency } = require('../../models/currency/currency.model');

typeorm.createConnection()
  .then(async connection => {
    const server = createServer({
      currencyRepository: connection.getRepository(Currency)
    });

    console.log("Starting server");
    server.listen(3000, () => {
      console.log("Server started");
    });
  })
  .catch(err => console.log(err))