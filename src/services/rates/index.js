require('dotenv').config();
const schedule = require('node-schedule');
const request = require('request-promise');
const { fetchRates } = require('./client/client');
const typeorm = require('typeorm');
const { Currency } = require('../../models/currency/currency.model');

process.on('uncaughtException', err => {
  console.log(err);
  process.exit(1);
});

process.on('unhandledRejection', err => {
  console.log(err);
  process.exit(1);
});

typeorm.createConnection()
  .then(async connection => {
    schedule.scheduleJob('*/10 * * * * * *', fetchRates(
      request,
      connection.getRepository(Currency)
    ));
  })
  .catch(err => console.log(err))
