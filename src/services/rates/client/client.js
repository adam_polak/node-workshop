const { Currency } = require('../../../models/currency/currency.model');
const { Rate } = require('../../../models/rate/rate.model');
const { v4 } = require('uuid');

const fetchRates = (http, currenciesRepository) => () => {
  http({
    uri: process.env.RATES_URI,
    headers: {
      'currency-api-token': process.env.RATES_TOKEN
    },
    json: true
  })
    .then(res => {
      console.log(res)

      const rates = res.rates.map(rate => new Rate(v4(), rate.currency, rate.rate));
      const currency = new Currency(v4(), res.currency, new Date(), rates);

      currenciesRepository.save(currency);
    })
    .catch(err => {
      console.log(err);
    })
};

module.exports = {
  fetchRates
};