const { EntitySchema } = require('typeorm');
const { Currency } = require('./currency.model');

module.exports = new EntitySchema({
  name: 'Currency',
  target: Currency,
  columns: {
    id: {
      primary: true,
      type: 'uuid'
    },
    code: {
      type: 'varchar',
      required: true
    },
    createDate: {
      type: 'timestamp'
    }
  },
  relations: {
    rates: {
      type: 'one-to-many',
      cascade: true,
      target: 'Rate',
      inverseSide: 'currency'
    }
  }
});