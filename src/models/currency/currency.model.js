class Currency {
  constructor(id, code, createDate, rates) {
    this.id = id;
    this.code = code;
    this.createDate = createDate;
    this.rates = rates;
  }
}

module.exports = {
  Currency
}