const { EntitySchema } = require('typeorm');
const { Rate } = require('./Rate.model');

module.exports = new EntitySchema({
  name: 'Rate',
  target: Rate,
  columns: {
    id: {
      primary: true,
      type: 'uuid'
    },
    code: {
      type: 'varchar',
      required: true
    },
    value: {
      type: 'decimal'
    }
  },
  relations: {
    currency: {
      type: 'many-to-one',
      target: 'Currency',
    }
  }
})