class Rate {
  constructor(id, code, value) {
    this.id = id;
    this.code = code;
    this.value = value;
  }
}

module.exports = {
  Rate
}